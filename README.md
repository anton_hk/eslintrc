# Purpose #

This is my suggested `.eslintrc` file for Visual Studio 2015-2017. 

If you use this file, Visual Studio will show you warnings about your JavaScript code that will help us maintain consistent coding standard.

`eslint` is the utility that analyzes code and shows errors when there are coding errors or formatting errors.

# How to use #

In Visual Studio, click `Tools` => `Web Code Analysis` => `Edit ESLint Settings`. Copy the content of the file from repository into the new window that opens. Save, and Visual Studio will now show you errors in the Error List.
